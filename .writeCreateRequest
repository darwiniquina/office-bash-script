#!/bin/bash

file_name="app/Http/Requests/${pascal_case_module}/Create${pascal_case_module}Request.php"

# Write the PHP code to the file
echo "<?php

namespace App\Http\Requests\\${pascal_case_module};

use App\Enums\Permissions;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class Create${pascal_case_module}Request extends FormRequest {
    public function authorize(): bool {
        \$permissions = json_decode(Auth::user()->permissions);

        \$can_create_${lower_case_module} = in_array(Permissions::${upper_case_module}_CREATE, \$permissions);

        return \$can_create_${lower_case_module};
    }

    protected function failedAuthorization() {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'message' => 'You do not have permission to create ${lower_case_module}.',
        ], 403));
    }

    public function rules(): array {
        return [
            'name' => 'required|string|max:255',
            'description' => 'nullable|string|max:255',
        ];
    }

    protected function failedValidation(Validator \$validator) {
        throw new HttpResponseException(response()->json([
            'success' => false,
            'message' => 'Validation errors',
            'data' => \$validator->errors(),
        ], 422));
    }
}" > $file_name