# == STYLE VARIABLES == #
  RED='\033[0;31m'
  GREEN='\033[0;32m'
  YELLOW='\033[0;33m'
  BLUE='\033[0;34m'
  MAGENTA='\033[0;35m'
  CYAN='\033[0;36m'
  ORANGE='\033[0;33m'
  PURPLE='\033[0;35m'
  GRAY='\033[0;90m'
  WHITE='\033[0;37m'
  LIGHT_RED='\033[1;31m'
  LIGHT_GREEN='\033[1;32m'
  LIGHT_BLUE='\033[1;34m'
  LIGHT_CYAN='\033[1;36m'
  BOLD='\033[1m'
  DIM='\033[2m'
  UNDERLINE='\033[4m'
  BLINK='\033[5m'
  REVERSE='\033[7m'
  HIDDEN='\033[8m'
  ITALIC='\033[3m'
  STRIKE='\033[9m'
  RESET='\033[0m'

  RANDOMEMOJI() {
    local my_array=("😊" "🥹" "🐼" "🔥" "🐇" "🐷" "🐔" "🐣" "💀" "🐧" "🦉" "🐭" "🍒" "🍆" "👽" "🐵" "🐸" "🐷" "🐯" "🐶" "🦍" "🥷" "🌊" "🍟" "🍕" "🦝" "🐙" "🏍️")
    local array_length=${#my_array[@]}
    local random_index=$((RANDOM % array_length))
    # COMMENT THIS ECHO TO REMOVE EMOJIS
    echo "${my_array[random_index]}"
  }

  alias c="clear"
  alias gs="git status"
# ===================================== #

# == PREPARATION BEFORE WORK == #
  function goto(){
    OPENDIRECTORY $1
  }

  function cdhpc() {
    OPENDIRECTORY "hpo"
    code .
  }

  function cdhp() {
    OPENDIRECTORY "hpo"
  }

  function nrw() {
    c
    PRINT "RUNNING" "NPM RUN WATCH"
    DASH
    OPENDIRECTORY "hpo"
    npm run watch
  }

  function socket() {
    c
    PRINT "RUNNING" "WEBSOCKET"
    DASH
    OPENDIRECTORY "socket"
    php artisan websocket:serve
  }

  function v3cd() {
    OPENDIRECTORY "v3api"
  }

  function v3cdc() {
    OPENDIRECTORY "v3api"
    code .
  }
# ===================================== #

# == QUEUE CODE == #
  function q() {
    RESETSOCKET
    clear 
    QUEUE
  }
  
  function qt() {
    RESETSOCKET
    clear 
    QUEUE "payroll-time-sheet"
  }

  function qsp() {
    RESETSOCKET
    clear 
    QUEUE "ssr-payslip"
  }
# ====================v3================= #

# == GIT COMMANDS FOR MASTER & DEVELOP == #
  function ng() {
    if [[ -n "$1" ]]; then
      PRINT "Changing Branching to " "$1" 
        git checkout -B $1;
      PRINT "Done ✓"
    fi
    RESET "master"
    PULL "master"
    PULL $(git rev-parse --abbrev-ref HEAD)
  }

  function gg() {
    PRINT "EXECUTING" "GOOD GAME"
    git add .
    if [[ -n "$1" ]]; then
      git commit -m "$1"
    else
      git commit -m "fix $(git rev-parse --abbrev-ref HEAD)"
    fi
    git push origin $(git rev-parse --abbrev-ref HEAD)
    PRINT "Done ✓"
  }

  function ngd() {
    PRINT "Reconstructing " "MAIN BRANCH & DEVELOP BRANCH"
      main_branch="$(git rev-parse --abbrev-ref HEAD)"

      # Your main branch contains the word develop
      if echo "$main_branch" | grep -q "develop"; then
          main_branch=$(echo "$main_branch" | sed 's/-develop.*//g')
      fi

      if [[ -n "$1" ]]; then
        develop_branch="$main_branch-develop-$1"
      else
        develop_branch="$main_branch-develop"
      fi
    PRINT "Done ✓"

    git checkout -B $develop_branch

    RESET "master"
    
    PULL "master"

    # FOR CHECKING ONLY!
    # WHEN CREATING DEVELOP much better if we only pull the MR in master branch
    PRINT "Pulling to" $develop_branch
    output=$(git pull origin $1 2>&1)

    if [[ $output =~ "fatal: couldn't find remote ref" ]]; then
      echo "Error: Remote reference not found."
      PRINT "Safe Walang nahatak na develop branch ✓"
    else
      ERROR "Dapat walang mahatak na develop branch"
      return
    fi

    PULL $main_branch
    PULL "develop"

    # DONT PUT AUTO PUSH HERE, BEACUSE WE NEED TO RESOLVE CONFLICT FIRST!
  }

  function ggd() {
    PRINT "Executing" "Pushing and creating merge request"
    branch=$(git rev-parse --abbrev-ref HEAD)
    git add .
    git commit -m "Push Develop $branch" 
    git push -o merge_request.create -o merge_request.target=develop origin $branch
    PRINT "Done ✓"
  }
# ===================================== #

# == PROD COMMANDS == #
  function prod() {
    if [ -z "$1" ]; then
      OPENDIRECTORY "prod-hpo"
      clear
      PRINT "CURRENT REPO" "ＨＩＥＲ ＰＡＹＲＯＬＬ ＯＬＤ（ＰＲＯＤＵＣＴＩＯＮ）"
    elif [ "$1" == "1" ]; then
      OPENDIRECTORY "prod-prod"
      clear
      PRINT "CURRENT REPO" "ＨＩＥＲ ＰＲＯＤ"
    fi
  }

  function build() {
    LOCATIONCHECKER "prod-hpo"
    if [ $? -ne 0 ]; then
      return
    fi

    ECHOBUILD $1
    DASH

    if [ -z "$1" ]; then
      PRINT "[1]" "DEVELOP"
      PRINT "[2]" "MASTER"
      PRINT "[3]" "MANUAL"
      PRINT "[4]" "SAME BRANCH"
      DASH
      read -p "MODE: " mode

      if [ -z "$mode" ]; then
        ERROR "Mode cannot be empty"
        return
      elif [[ "$mode" == "1" || "$mode" == "2" ]]; then
        OPENDIRECTORY "prod-prod"
        dev_or_v1_branch_name=$([ "$mode" = "1" ] && echo "dev" || echo "v1")

        # Fetch updates from remote origin
        git fetch origin

        # List all branches on the remote origin
        branches=$(git branch -r | grep "$dev_or_v1_branch_name")

        # Check if any branches with "dev or v1" were found
        if [[ -z "$branches" ]]; then
          ERROR "No branches found with 'dev or v1' in their name."
          return
        fi

        # Get the latest branch with "dev or v1" using lexicographic sorting (alphabetical)
        latest_dev_branch=$(echo "$branches" | grep -Eo '[^/]+$' | sort -rV | head -n 1)
        
        # Without the last
        base_string="${latest_dev_branch%.*}"
        last_number="${latest_dev_branch##*.}"
        incremented_number=$((last_number + 1))

        if [ "$incremented_number" -eq 100 ]; then
          ERROR "Incremented version number reached 100, which is not supported yet."
          return
        fi
        
        new_branch="${base_string}.${incremented_number}"
        PRINT "PREVIOUS BRANCH: " "$latest_dev_branch"
        PRINT "NEXT BRANCH TO CREATE: " "$new_branch"
        OPENDIRECTORY "prod-hpo"
        branchname=$new_branch

      elif [ "$mode" == "3" ]; then
        read -p "BRANCH NAME: " branchname
        if [ -z "$branchname" ]; then
            PRINT "Error" "Branch name cannot be empty. Aborting."
            return
        fi
      elif [ "$mode" == "4" ]; then
        branchname=$(git rev-parse --abbrev-ref HEAD)
      fi
    else
        branchname=$(git rev-parse --abbrev-ref HEAD)
    fi

    PRINT "CHECKING OUT TO " "$branchname"
      git checkout -B $branchname
    echo -e "${GREEN}Done ✓${RESET}\n"

    RESET "master"

    PRINT "PULLING" "master"
    strict_pull_master=$(git pull origin "master" 2>&1)
    # Check if the output contains the word "CONFLICT" (case-insensitive)
    printf "${CYAN}%b${RESET}\n" "$strict_pull_master"
    if [[ $strict_pull_master =~ [Cc][Oo][Nn][Ff][Ll][Ii][Cc][Tt] ]]; then
        ERROR "Conflict detected in pull output"
        return
    fi
    PRINT "Done ✓"

    if [[ $branchname == *"dev"* ]]; then
      PRINT "PULLING" "develop"
      strict_pull_develop=$(git pull origin "develop" 2>&1)
      printf "${CYAN}%b${RESET}\n" "$strict_pull_develop"
      if [[ $strict_pull_develop =~ [Cc][Oo][Nn][Ff][Ll][Ii][Cc][Tt] ]]; then
          ERROR "Conflict detected in pull output"
          return
      fi
      PRINT "Done ✓"
    fi

    PRINT "Changing Welcome.vue branch from $(sed -n "43p" resources/assets/js/components/Welcome.vue | tr -s " ") to " "Hier Payroll $branchname"
      sed -i "43s/.*/          Hier Payroll $branchname/" resources/assets/js/components/Welcome.vue 
    PRINT "Done ✓"

    PRINT "Running" "Deployment Script"
      ./deploy.sh 1
    PRINT "Done ✓"

    PRINT "Copying public.zip into " "hier-prod directory"
      cp _RELEASES/release/public.zip ../hier-prod 
    PRINT "Done ✓"

    if [[ $branchname != *"dev"* ]]; then
      PRINT "Copying public.zip into " "RELEASES/versions directory"
      cp _RELEASES/release/public.zip _RELEASES/versions
      PRINT "Done ✓"

      PRINT "Renaming public.zip into " "$branchname.zip"
        mv _RELEASES/versions/public.zip "_RELEASES/versions/$branchname.zip"
      PRINT "Done ✓"
    fi

    DASH

    PRINT "Latest build" "$branchname"
    echo -e "${YELLOW}${BLINK}${BOLD}${UNDERLINE}$(stat --format="%x" _RELEASES/release/public.zip | awk '{split($0,a," "); split(a[1],b,"-"); split(a[2],c,":"); printf "%s/%s/%s %02d:%02d %s\n", b[2], b[3], b[1], (c[1] > 12 ? c[1]-12 : (c[1] == 0 ? 12 : c[1])), c[2], (c[1] >= 12 ? "PM" : "AM")}')${RESET}"

    # HIER PROD
    if [ -f "README.md" ]; then
      echo "README.md File exists, the README.md was removed."
      rm README.md
    fi

    OPENDIRECTORY "prod-prod"

    # Checking ng current branch sa prod if same pa ba sila ng branch hierpayroll-old prod
    current_branch=$(git branch --show-current)
    if [ "$current_branch" != $branchname ]; then
        PRINT "Updating your branch into " "$branchname"
        git checkout -B $branchname
    fi

    PRINT "Running " "PUSH TO PROD"

    git add . 
    git commit -m $branchname
    git push origin $branchname
    
    PRINT "Done ✓"
    OPENDIRECTORY "prod-hpo"
    SUCCESS
  }
# ===================================== #

# == RARELY USED COMMANDS == #
  function cleandb() {
    default="hbsi.hbsi"
    php -r "
      require 'vendor/autoload.php';
      use Illuminate\Support\Facades\DB;
      use Illuminate\Support\Facades\Schema;
      use App\Employee;

      \$app = require_once __DIR__.'/bootstrap/app.php';
      \$app->make('Illuminate\Contracts\Console\Kernel')->bootstrap();
      Schema::dropIfExists('audits');
      DB::table('migrations')->where('migration', 'like', '%AUDITS%')->delete();

      \$output = shell_exec('php artisan migrate 2>&1');
      echo \$output;

      DB::statement('ALTER TABLE audits ADD company_id BIGINT UNSIGNED');

      DB::table('mail_setups')->update(['smtp'=>null, 'port'=>null,'username' => null, 'password' => null, 'is_mail_env' => 1]);
      DB::table('employees')->update(['email' => null, 'password' => null]);
      DB::table('users')->where('id', '1')->update(['password' => bcrypt('secret')]);
      DB::table('company_infos')->update(['email' => null]);

      \$companies = DB::table('company_infos')->pluck('id');
      \$last_employees = \$companies->map(function (\$company) {
          return DB::table('employees')->where('company_id', \$company)->max('id');
      })->filter()->values()->all();

      \$created_emails = [];
      foreach (\$last_employees as \$last_employee_id) {
        \$company_id = DB::table('employees')->select('company_id')->where('id', \$last_employee_id)->pluck('company_id')->first();
        \$company_name = DB::table('company_infos')->select('registered_name')->where('id', \$company_id)->pluck('registered_name')->first();
        \$role_id = DB::table('roles')->select('id')->where('company_id', \$company_id)->pluck('id')->first();

        \$company_ids = DB::table('company_infos')->pluck('id')->all();
        foreach(\$company_ids as \$company_id){
          \$role_id = DB::table('ess_roles')->where('company_id', \$company_id)->value('id');

          DB::table('ess_assigned_roles')->updateOrInsert(
            ['entity_id' => \$last_employee_id],
            ['role_id' => \$role_id, 'entity_type' => 'App\Employee' ]
          );
        
          App\Employee::find(\$last_employee_id)->update([
            'email' =>  '$default' . \$last_employee_id . '@gmail.com',
            'is_locked' => 0,
            'active' => 1,
            'is_ess_active' => 1,
            'password' => bcrypt('$default'),
            'ess_role_id' => \$role_id
          ]);

        }

        \$created_emails[] =  [
          'employee_id' => \$last_employee_id,
          'employee_company' => \$company_name,
          'employee_email' => '$default' . \$last_employee_id . '@gmail.com',
          'employee_password' => '$default'
        ];
        
      }

      echo \" \n \";
      echo 'DUMMY EMPLOYEES' .\"\n\";
      echo '___________________________________________' . \"\n\n\";
      foreach(\$created_emails as \$ce){
        echo 'EMPLOYEE ID: ' . \$ce['employee_id'] . \"\n\";
        echo 'COMPANY ID: ' . \$ce['employee_company']. \"\n\";
        echo 'EMAIL: ' . \$ce['employee_email']. \"\n\";
        echo 'PASSWORD: ' . \$ce['employee_password'] . \"\n\";
        echo \"\n\";
      }
    "
  }

  function ess() {
    php -r "
      require 'vendor/autoload.php';
      use Illuminate\Support\Facades\DB;
      use Illuminate\Support\Facades\Schema;
      use App\Employee;

      \$app = require_once __DIR__.'/bootstrap/app.php';
      \$app->make('Illuminate\Contracts\Console\Kernel')->bootstrap();
      
      \$employees = Employee::with('companyInfos')->where('password', '!=', null)->get();

      echo 'ACCOUNTS' . \"\n\";
      echo '___________________________________________' . \"\n\n\";
      foreach(\$employees as \$employee) {
        echo 'EMPLOYEE ID: ' . \$employee['id'] . \"\n\";
        echo 'COMPANY NAME: ' . \$employee['companyInfos']['registered_name']. \"\n\";
        echo 'EMAIL: ' . \$employee['email']. \"\n\";
        echo \"\n\";
      }
      
    "
  }
# ===================================== #

# == V3 COMMANDS == #
  function v3rdb() {
    php artisan tenant:migrate-fresh
    php artisan tenant:seed
  } 

  function pam() {
    php artisan make:"$@"
  }

  function v3module(){
    read -p "Module name: " module_name
    lower_case_module=$(to_lowercase "$module_name")
    upper_case_module=$(to_upper_case "$module_name")
    pascal_case_module=$(to_pascal_case "$module_name")

    PRINT "CREATING" "CONTROLLER"
      pam "controller" "${module_name}Controller"
    PRINT "Done ✓"

    PRINT "CREATING" "REQUEST"
      pam "request" "${pascal_case_module}/Get${pascal_case_module}Request"
      pam "request" "${pascal_case_module}/Create${pascal_case_module}Request"
      pam "request" "${pascal_case_module}/Update${pascal_case_module}Request"
      pam "request" "${pascal_case_module}/Destroy${pascal_case_module}Request"
    PRINT "Done ✓"

    PRINT "CREATING" "MIGRATION"
      pam "migration" "create ${lower_case_module}s"
    PRINT "Done ✓"

    PRINT "CREATING" "SEEDER"
      pam "seeder" "${pascal_case_module}Seeder/${pascal_case_module}Seeder"
    PRINT "Done ✓"

    PRINT "EXECUTING" "Seeds"
      source .writeApiRoute
      source .writeController
      source .writeGetRequest
      source .writeCreateRequest
      source .writeDestroyRequest
      source .writeUpdateRequest
      source .writeSeeder
      source .writePermission
    PRINT "Done ✓"

  PRINT "Created:"
  echo "1. ${upper_case_module} Controller with CRUD methods"
  echo "2. GET, CREATE, UPDATE, DELETE Requests for ${upper_case_module}"
  echo "3. Migration for ${upper_case_module} with default template"
  echo "4. Seeder for ${upper_case_module} with default template"
  echo "5. The seeder was also added in the Tenant Database Seeder"

  echo "Now move the created migration inside of the migrations/tenant folder"
  echo "Configure your database seeder and migration"
  echo "Run v3rdb"

  }

# ===================================== #

# == HELPER FUNCTIONS == #
  # FOR PRINTINGS
    function PRINT() {
      sentence=$1
      highlight=$2

      if [ -z "$highlight" ]; then
        echo -e "${BOLD}${GREEN}${BLINK}$sentence ${RESET}"$(RANDOMEMOJI)
      else
        echo -e "\n${BOLD}$sentence${GREEN}${BLINK} $highlight ${RESET}"$(RANDOMEMOJI)
      fi
    }

    function SUCCESS() {
      PRINT "Script executed successfully!"
      start "" "$HOME/office-bash-script/success.vbs"
    }

    function ERROR() {
      msg=$1
      echo -e "\n${BOLD}${RED}${BLINK}ERROR: ${RESET}$msg "$(RANDOMEMOJI)
      start "" "$HOME/office-bash-script/error.vbs"
    }

    function DASH() {
      echo -e ${LIGHT_BLUE}-----------------------------------------------${RESET}
    }
    
    function ECHOBUILD() {
      clear && 
      if [ -z "$1" ]; then
        PRINT "ＢＵＩＬＤ " "ＯＰＴＩＯＮＳ"
      elif [ "$1" == "1" ]; then
        PRINT "ＢＵＩＬＤ １ " "ＳＡＭＥ ＢＲＡＮＣＨ"
      else
        clear
        PRINT "Error " "Hindi support, tabi mainet!"
        return
      fi
    }

  function to_lowercase() {
    local input_string="$1"
    echo "$input_string" | tr '[:upper:]' '[:lower:]'
  }

  function to_upper_case(){
    local input_string="$1"
    upper_case=$(echo "$1" | tr '[:lower:]' '[:upper:]')
    echo "$upper_case"
  }

  function to_pascal_case() {
    local input_string="$1"
    local pascal_case=$(echo "$input_string" | sed -r 's/(^|_|-)([a-z])/\U\2/g' | sed -r 's/[ _-]//g')
    echo "$pascal_case"
  }
  # ============

  # BRANCH MANAGING 
    function RESET() {
      PRINT "Reseting to" $1
        git reset --hard origin/$1
      PRINT "Done ✓"
    }

    function PULL() {
      PRINT "Pulling to" $1
        git pull origin $1
      PRINT "Done ✓"
      
    }
  # ============  

  # DIRECTORY MANAGING
    function OPENDIRECTORY(){
      cd ~
      current_directory=$(pwd)
      local directory=""
      case "$1" in
        "hpo")
          directory='/c/laragon/www/hierpayroll-old'
          ;;
        "prod-hpo")
          directory="/c/laragon/www/production/hierpayroll-old"
          ;;
        "prod-prod")
          directory="/c/laragon/www/production/hier-prod"
          ;;
        "socket")
          directory="/c/laragon/www/socket"
          ;;
        "v3api")
          directory="/c/projects/hierpayroll-v3-api"
          ;;
        "bash")
          directory="$HOME/office-bash-script/"
          ;;
        *)
        return 1
        ;;
      esac
      cd $directory
    }

    function LOCATIONCHECKER(){
      current_directory=$(pwd)
      local directory=""
      case "$1" in
        "hpo")
          directory='/c/laragon/www/hierpayroll-old'
          ;;
        "prod-hpo")
          directory="/c/laragon/www/production/hierpayroll-old"
          ;;
        "prod-prod")
          directory="/c/laragon/www/production/hier-prod"
          ;;
        *)
        return 1
        ;;
      esac

      if [ "$current_directory" != $directory ]; then
        clear
        ERROR "WRONG DIRECTORY" 
        return 1
      fi
    }

  # ============

  # FOR QUEUE TASKS
    function RESETSOCKET() {
        php -r "
            require 'vendor/autoload.php';
            use Illuminate\Support\Facades\DB;
            \$app = require_once __DIR__.'/bootstrap/app.php';
            \$app->make('Illuminate\Contracts\Console\Kernel')->bootstrap();
            DB::table('socket_processes')->update(['current_count' => 1, 'total_count' => 1]);
        "
    }

    function QUEUE() {
        if [ -z "$1" ]; then
          PRINT "QUEUE" "NORMAL"
          php artisan queue:work --tries=1
        elif [ "$1" == "payroll-time-sheet" ]; then
          PRINT "QUEUE" "PAYROLL TIME SHEET"
          php artisan queue:work --tries=1 --queue=payroll-timesheet
        elif [ "$1" == "ssr-payslip" ]; then
          PRINT "QUEUE" "SSR PAY SLIP"
          php artisan queue:work --tries=1 --queue=ssr-payslip
        fi


    }
  # ============
# ===================================== #

