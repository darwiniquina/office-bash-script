# SOME COMMANDS
**c** - clear
**gs** - git status

# PREPARATION COMMANDS BEFORE WORK
**cdhpc** - Opens `hier-payroll-old` then open in `Visual studio Code`
**cdhp** - Opens `hier-payroll-old` directory
**nrw** - Opens `hier-payroll-old` `npm run watch`
**socket** - Opens `socket repo` and runs `php artisan web:socket serve`

# QUEUE COMMANDS
**q** - Normal Queue
**qt** - Payroll Time Sheet Queue
**qsp** - Payslip SSR Queue
**q** `<SAMPLE QUEUE>` - A Queue that is not QT or QSP

# GIT COMMANDS FOR MASTER & DEVELOP
#### NORMAL COMMANDS
**ng** - Use this when working on new branch, or resetting to your current branch
`Usage: ng or ng <ANOTHER-BRANCH>`

**gg** - This will push your work into your current branch that you are currently in
`Usage: gg or gg <MY-MODIFIED-COMMENT>`

#### DEVELOP COMMANDS
**ngd** - When creating a `develop` branch for your current `master` branch
`Usage: ngd or ngd <DIFFERENT-VERSION-1>`

**ggd** - This pushes the branch and also creates a merge request to develop waiting to be merged
	Use this after NGD for now, need more testings to support in different scenarios.
`Usage: ggd`

# PRODUCTION COMMANDS

**prod** - Opens the production > `hier-payroll-old`
**prod 1** - Opens the production > `hier-prod`

**build** - Builds a production file you can choose between "develop","master", "manual", "same-branch", that will be pushed in `hier-prod git lab repository`, This will also automatically save a copy if it is a `v1` branch or so called master
**build 1** - Builds a production file in a same branch that will be pushed in `hier-prod git lab repository`

# RARELY USED COMMANDS

**newdb** - When you have a new DB to reset some data
Removes
 - Creates `audits` table
 - Remove `mail_setup` Emails
 - Remove `employees` Emails and Passwords
 - Change the User's default account's password into `secret`
 - Gives you a ESS dummy account in different companies with a given `email` and given `password`
 - Assigns a default role on each Dummy account of ESS
 - Gives you a list of ESS dummy account that you can login, with a given `email` and `password`